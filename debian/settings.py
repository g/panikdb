# panikdb configuration files
# this file can be edited but settings are better placed in settings.d/ files

import glob

DEBUG = False

LANGUAGE_CODE = 'fr-be'
TIME_ZONE = 'Europe/Brussels'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyMemcacheCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'panikdb',
    },
}

ALLOWED_HOSTS = ()

STATIC_ROOT = '/var/lib/panikdb/static/'
MEDIA_ROOT = '/var/lib/panikdb/media/'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'request_context': {
            '()': 'panikdb.logger.RequestContextFilter',
        },
    },
    'formatters': {
        'verbose': {
            'format': '{asctime} ({levelname:.1s}) [{username}] {message}',
            'style': '{',
        },
        'verbose-notime': {
            'format': ' ({levelname:.1s}) [{username}] {message}',
            'style': '{',
        },
        'simple': {
            'format': '({levelname:.1s}) {message}',
            'style': '{',
        },
        'stamina-verbose': {
            'format': '{asctime} ({levelname:.1s}) {message}',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
            'level': 'INFO',
        },
        'panikdb-syslog': {
            'level': 'DEBUG',
            'class': 'logging.handlers.SysLogHandler',
            'facility': 'local6',
            'address': '/dev/log',
            'formatter': 'verbose-notime',
        },
        'stamina-file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': '/var/log/stamina/stamina.log',
            'when': 'w0',
            'formatter': 'stamina-verbose',
        },
    },
    'root': {
        'handlers': ['console'],
        'level': 'WARNING',
    },
    'loggers': {
        'switch-proxy': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'stamina': {
            'handlers': ['console', 'stamina-file'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'panikdb': {
            'handlers': ['panikdb-syslog'],
            'filters': ['request_context'],
            'level': 'DEBUG',
            'propagate': False,
        },
    },
}

for filename in sorted(glob.glob('/etc/panikdb/settings.d/*.py')):
    exec(open(filename).read())
