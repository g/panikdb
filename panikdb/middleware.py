import threading

from django.utils.deprecation import MiddlewareMixin


class StoreRequestMiddleware(MiddlewareMixin):
    collection = {}

    def process_request(self, request):
        StoreRequestMiddleware.collection[threading.currentThread()] = request

    def process_response(self, request, response):
        StoreRequestMiddleware.collection.pop(threading.currentThread(), None)
        return response

    @classmethod
    def get_request(cls):
        return cls.collection.get(threading.currentThread())
