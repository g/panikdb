import combo.public.views
from agendas.urls import urlpatterns as agendas_urlpatterns
from ckeditor.views import browse as ckeditor_browse
from ckeditor.views import upload as ckeditor_upload
from combo.manager.urls import urlpatterns as combo_manager_urls
from combo.public.views import snapshot as combo_snapshot_view
from django.conf import settings
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.urls import include, path, re_path, reverse_lazy
from django.views.generic import RedirectView
from emissions.models import Emission, Episode, NewsItem
from emissions.urls import urlpatterns as emissions_urlpatterns
from matos.urls import urlpatterns as matos_urlpatterns
from newsletter.urls import management_patterns as newsletter_urlpatterns
from nonstop.urls import public_urlpatterns as nonstop_public_urlpatterns
from nonstop.urls import urlpatterns as nonstop_urlpatterns
from panikombo.urls import urlpatterns as panikombo_urlpatterns
from wiki.urls import urlpatterns as wiki_urlpatterns

from . import views
from .aa.urls import urlpatterns as aa_urlpatterns
from .forms import AuthenticationForm
from .oauth.urls import urlpatterns as oauth_urlpatterns
from .oauth.views import well_known_openid_configuration
from .pige.urls import urlpatterns as pige_urlpatterns
from .poll import views as poll_views
from .regie.urls import urlpatterns as regie_urlpatterns
from .service_messages.urls import urlpatterns as service_messages_urlpatterns
from .urls_utils import cms_permission_required, decorated_includes

urlpatterns = [
    path('', views.home, name='home'),
    re_path(r'^ckeditor/upload/', login_required(ckeditor_upload)),
    re_path(r'^ckeditor/browse/', login_required(ckeditor_browse)),
    re_path(r'^search/', login_required(views.search), name='panikdb-search'),
    re_path(r'^emissions/', decorated_includes(login_required, include(emissions_urlpatterns))),
    re_path(r'^matos/', decorated_includes(login_required, include(matos_urlpatterns))),
    re_path(r'^nonstop/', decorated_includes(login_required, include(nonstop_urlpatterns))),
    path('nonstop/', include(nonstop_public_urlpatterns)),
    path('newsletters/add', views.newsletter_create, name='newsletter-create'),
    re_path(r'^newsletters/', decorated_includes(login_required, include(newsletter_urlpatterns))),
    re_path(r'^cms/', decorated_includes(cms_permission_required, include(combo_manager_urls))),
    re_path(
        r'^cms-snapshot/(?P<pk>\w+)/$',
        cms_permission_required(combo_snapshot_view),
        name='combo-snapshot-view',
    ),
    re_path(r'^focus/set/(?P<object_type>[\w,-]+)/(?P<object_id>\d+)$', views.focus_set, name='focus-set'),
    re_path(
        r'^focus/unset/(?P<object_type>[\w,-]+)/(?P<object_id>\d+)$', views.focus_unset, name='focus-unset'
    ),
    path(
        'accounts/login/',
        auth_views.LoginView.as_view(authentication_form=AuthenticationForm),
        name='login',
    ),
    re_path(r'^accounts/logout/', auth_views.logout_then_login, name='logout'),
    path(
        'password/change/',
        auth_views.PasswordChangeView.as_view(),
        name='password_change',
    ),
    path(
        'password/change/done/',
        auth_views.PasswordChangeDoneView.as_view(),
        name='password_change_done',
    ),
    path(
        'accounts/password_reset/',
        auth_views.PasswordResetView.as_view(
            email_template_name='registration/password_reset_email.html',
        ),
        name='password_reset',
    ),
    path(
        'accounts/password_reset/done/',
        auth_views.PasswordResetDoneView.as_view(),
        name='password_reset_done',
    ),
    path(
        'accounts/reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm',
    ),
    path(
        'accounts/reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'
    ),
    path('vote/', poll_views.vote, name='vote'),
    path('vote/results/', poll_views.vote_results, name='vote-results'),
    re_path(
        r'^ajax/cell/(?P<page_pk>\d+)/(?P<cell_reference>[\w_]+-\d+)/$',
        combo.public.views.ajax_page_cell,
        name='combo-public-ajax-page-cell',
    ),
    re_path(r'^agendas/', decorated_includes(login_required, include(agendas_urlpatterns))),
    path('panikombo/', include(panikombo_urlpatterns)),
    path('wiki/', include(wiki_urlpatterns)),
    path('pige/', include(pige_urlpatterns)),
    path('regie/', include(regie_urlpatterns)),
    path('oauth/', include(oauth_urlpatterns)),
    re_path(r'^\.well-known/openid-configuration$', well_known_openid_configuration),
    path('messages/', include(service_messages_urlpatterns)),
    path('system/', views.system),
    re_path(r'^admin/', admin.site.urls),
]

urlpatterns += aa_urlpatterns

from combo import plugins

urlpatterns = plugins.register_plugins_urls(urlpatterns)

from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns += staticfiles_urlpatterns()

from django.conf.urls.static import static

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG_TOOLBAR:
    urlpatterns += [
        path('__debug__/', include('debug_toolbar.urls')),
    ]
