import django
from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied

if django.VERSION < (2, 0, 0):
    from django.urls.resolvers import RegexURLPattern as URLPattern
    from django.urls.resolvers import RegexURLResolver as URLResolver
else:
    from django.urls.resolvers import URLPattern, URLResolver


class DecoratedURLPattern(URLPattern):
    def resolve(self, *args, **kwargs):
        result = super().resolve(*args, **kwargs)
        if result:
            result.func = self._decorate_with(result.func)
        return result


class DecoratedURLResolver(URLResolver):
    def resolve(self, *args, **kwargs):
        result = super().resolve(*args, **kwargs)
        if result:
            result.func = self._decorate_with(result.func)
        return result


def decorated_includes(func, includes, *args, **kwargs):
    urlconf_module, app_name, namespace = includes

    for item in urlconf_module:
        if isinstance(item, URLResolver):
            item.__class__ = DecoratedURLResolver
        else:
            item.__class__ = DecoratedURLPattern
        item._decorate_with = func

    return urlconf_module, app_name, namespace


def cms_permission_required(function=None, login_url=None):
    def check_cms_permission(user):
        if user and user.has_perm('data.add_page'):
            return True
        if user and not user.is_anonymous:
            raise PermissionDenied()
        # As the last resort, show the login form
        return False

    actual_decorator = user_passes_test(check_cms_permission, login_url=login_url)
    if function:
        return actual_decorator(function)
    return actual_decorator
