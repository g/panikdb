$(function() {
  $('.service-message h2:first-child, .service-message h3:first-child').on('click', function() {
    $(this).nextAll().toggle();
    if (window.localStorage) {
      if (window.localStorage.getItem('service-message') == 'toggled') {
        window.localStorage.setItem('service-message', 'untoggled');
      } else {
        window.localStorage.setItem('service-message', 'toggled');
      }
    }
  });
  if (window.localStorage && window.localStorage.getItem('service-message') == 'toggled') {
    $('.service-message h2:first-child, .service-message h3:first-child').trigger('click');
    window.localStorage.setItem('service-message', 'toggled');
  }
  $('.service-message').show();
});
