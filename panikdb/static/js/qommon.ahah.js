/* Via
   https://developer.mozilla.org/en-US/docs/Web/Guide/DOM/Manipulating_the_browser_history
   http://simeonfranklin.com/blog/2011/aug/22/ahah-django-and-jquery/
   http://rosspenman.com/pushstate-jquery/
*/

$(function() {

    function ajax_load(html) {
        $(this).children(':first').unwrap();
        init();
    }

    function loadpage(href) {
        console.log('loadpage', href);
        $('#wrap').load(href + " #wrap", ajax_load);
    }

    function init() {
        $('a:not([href^="http"])').click(function() {
            if ($(this).attr('href') == '#')
                return true;
            var href = this.getAttribute('href');
            loadpage(href);
            history.pushState({'href': href}, '', href);
            return false;
        });
    }

    init();

    $(window).on("popstate", function(e) {
        console.log('popstate', e.originalEvent.state);
        if (e.originalEvent.state !== null) {
            console.log('href:', location.href);
            loadpage(location.href);
        }
    });
});
