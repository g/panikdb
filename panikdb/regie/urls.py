from django.urls import path

from . import views

urlpatterns = [
    path('', views.regie_home, name='regie-home'),
    path('tracks/', views.regie_tracks, name='regie-tracks'),
    path('tracks/search/', views.regie_tracks_search, name='regie-tracks-search'),
    path('jingles/search/', views.regie_jingles_search, name='regie-jingles-search'),
    path('playing/', views.playing, name='regie-playing'),
    path('switch/', views.switch, name='regie-switch'),
]
