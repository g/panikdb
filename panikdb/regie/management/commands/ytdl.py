import glob
import json
import os

import yt_dlp
from django.conf import settings
from django.core.files.storage import default_storage
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, verbosity, **kwargs):
        self.verbosity = verbosity
        self.ytdl_path = default_storage.path('ytdl')
        if not os.path.exists(self.ytdl_path):
            os.mkdir(self.ytdl_path)
        while True:
            done = []
            for filename in glob.glob(os.path.join(self.ytdl_path, '*.json')):
                with open(filename) as fd:
                    data = json.load(fd)

                if data.get('_status') == 'finished' or data.get('_error'):
                    continue

                self.ytdl(filename, data)
                done.append(filename)
            if not done:
                # nothing
                break

    def ytdl(self, filename, data):
        url = data['_url']
        if self.verbosity:
            print(url)

        def progress_hook(d):
            data['_percent_str'] = d.get('_percent_str')
            data['_status'] = d.get('status')
            with open(filename, 'w') as fd:
                json.dump(data, fp=fd, indent=2)

        ydl_opts = {
            'quiet': True,
            'noplaylist': True,
            'format': 'bestaudio',
            'outtmpl': os.path.join(self.ytdl_path, '%(id)s.%(ext)s'),
            'updatetime': False,
            'postprocessors': [
                {
                    'key': 'FFmpegExtractAudio',
                    'preferredcodec': 'opus',
                }
            ],
            'progress_hooks': [progress_hook],
        }
        with yt_dlp.YoutubeDL(ydl_opts) as ydl:
            ydl.download([url])

        if data.get('_status') == 'finished':
            base_filename = os.path.splitext(filename)[0]
            real_filename = [
                x
                for x in glob.glob(base_filename + '*')
                if not x.endswith('.part') and not x.endswith('.json')
            ][0]
            url = os.path.join(settings.MEDIA_URL, 'ytdl', real_filename[len(self.ytdl_path) + 1 :])
            data['_audio_url'] = url
            with open(filename, 'w') as fd:
                json.dump(data, fp=fd, indent=2)
        else:
            data['_error'] = 'XXX'
            with open(filename, 'w') as fd:
                json.dump(data, fp=fd, indent=2)
