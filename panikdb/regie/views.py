import datetime
import glob
import json
import logging
import os
import socket
import subprocess

import requests
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm
from django.core.files.storage import default_storage
from django.db.models import Q
from django.http import HttpResponseForbidden, JsonResponse
from django.urls import reverse
from django.views.generic.base import TemplateView
from emissions.models import Nonstop
from nonstop.models import Jingle, SomaLogLine, Track
from nonstop.utils import switch_audio_source

from panikdb.context_processors import internal_ip
from panikdb.pige.views import PigeDownloadMixin
from panikdb.service_messages.models import Message

logger = logging.getLogger('panikdb')


def is_regie_computer(request):
    return internal_ip(request).get('current_ip') in settings.REGIE_IPS


class RegieHome(TemplateView, PigeDownloadMixin):
    template_name = 'regie-home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.get_pige_context_data())
        context['login_form'] = AuthenticationForm()

        context['service_message'] = Message.objects.all().first()
        context['nonstop_zones'] = Nonstop.objects.all()
        current_time = datetime.datetime.now().time()
        for zone in context['nonstop_zones']:
            if (zone.start < zone.end and (current_time >= zone.start and current_time < zone.end)) or (
                zone.start > zone.end and (current_time >= zone.start or current_time < zone.end)
            ):
                context['current_nonstop_zone'] = zone
                break

        if (
            getattr(settings, 'NONSTOP_ON_AIR_SWITCH_URL', None)
            or getattr(settings, 'NONSTOP_JACK_SWITCH_SEND_UDP_NOTIFICATIONS', None)
        ) and getattr(self, 'request', None):
            context['is_regie_computer'] = is_regie_computer(self.request)
            context['has_switch'] = True

        return context


regie_home = RegieHome.as_view()


def json_tracks(tracks):
    return JsonResponse(
        {
            'data': [
                {
                    'id': track.id,
                    'title': track.title,
                    'artist': track.artist.name if track.artist_id else None,
                    'instru': track.instru,
                    'language': track.language,
                    'cfwb': track.cfwb,
                    'duration': '%d:%02d'
                    % (track.duration.total_seconds() / 60, track.duration.total_seconds() % 60),
                }
                for track in tracks
            ]
        }
    )


def regie_tracks(request):
    now = datetime.datetime.now()

    tracks = Track.objects.filter(duration__isnull=False)

    current_zone = None
    if request.GET.get('zone') == 'any':
        pass
    elif request.GET.get('zone'):
        current_zone = Nonstop.objects.get(slug=request.GET.get('zone'))
    else:
        for nonstop in Nonstop.objects.all():
            if (
                nonstop.start < nonstop.end and (now.time() >= nonstop.start and now.time() < nonstop.end)
            ) or (nonstop.start > nonstop.end and (now.time() >= nonstop.start or now.time() < nonstop.end)):
                current_zone = nonstop
                break

    if current_zone:
        tracks = tracks.filter(nonstop_zones__in=[current_zone.id])

    if request.GET.get('recent') == 'yes':
        tracks = tracks.filter(creation_timestamp__gt=now - datetime.timedelta(days=30))

    if request.GET.get('lang') in ('fr', 'nl', 'en'):
        tracks = tracks.filter(language=request.GET.get('lang'))
    elif request.GET.get('lang') == 'instru':
        tracks = tracks.filter(instru=True)

    if request.GET.get('duration') == 'short':
        tracks = tracks.filter(duration__lt=datetime.timedelta(minutes=2, seconds=0))
    elif request.GET.get('duration') == 'average':
        tracks = tracks.filter(duration__gte=datetime.timedelta(minutes=2, seconds=0))
        tracks = tracks.filter(duration__lt=datetime.timedelta(minutes=5, seconds=0))
    elif request.GET.get('duration') == 'long':
        tracks = tracks.filter(duration__gte=datetime.timedelta(minutes=5, seconds=0))
    tracks = tracks.order_by('?')

    return json_tracks(tracks[:5])


def regie_tracks_search(request):
    q = request.GET['q']
    if q.startswith('https://'):
        ytdl_path = default_storage.path('ytdl')
        if not os.path.exists(ytdl_path):
            os.mkdir(ytdl_path)
        # make a first run with just metadata, this is to obtain the filename
        # and use a file that was previously downloaded.
        proc = subprocess.run(
            [
                'yt-dlp',
                '--extract-audio',
                '--no-playlist',
                '--no-mtime',
                '--dump-json',
                '--audio-format',
                'opus',
                '-o',
                ytdl_path + '/%(id)s.%(ext)s',
                q,
            ],
            capture_output=True,
        )
        output = json.loads(proc.stdout)
        filename = output['_filename']
        # audio conversion may be done by youtube-dl and filename would still
        # contain the original filename, look for files with same basename,
        # regardless of the extension.
        base_filename = os.path.splitext(filename)[0]
        try:
            real_filename = [
                x
                for x in glob.glob(base_filename + '*')
                if not x.endswith('.part') and not x.endswith('.json')
            ][0]
        except IndexError:
            # not yet downloaded, store json for async downloading with the ytdl
            # management command
            ready = False
            real_filename = os.path.splitext(filename)[0] + '.json'
            output['_url'] = q
            with open(real_filename, 'w') as fd:
                json.dump(output, fp=fd, indent=2)
        else:
            ready = True
        url = os.path.join(settings.MEDIA_URL, 'ytdl', real_filename[len(ytdl_path) + 1 :])
        title = output['title']
        duration = '%d:%02d' % (int(output['duration']) / 60, int(output['duration']) % 60)
        good_filename = '%s%s' % (title, os.path.splitext(real_filename)[-1])
        return JsonResponse(
            {
                'data': [
                    {
                        'title': title,
                        'duration': duration,
                        'url': url,
                        'filename': good_filename,
                        'ready': ready,
                    }
                ]
            }
        )

    tracks = Track.objects.filter(duration__isnull=False).filter(
        Q(title__icontains=q.lower()) | Q(artist__name__icontains=q.lower())
    )
    tracks = tracks.order_by('?')
    return json_tracks(tracks[:5])


def regie_jingles_search(request):
    q = request.GET['q']
    jingles = Jingle.objects.filter(duration__isnull=False)
    if q:
        jingles = jingles.filter(label__icontains=q.lower())
    jingles = jingles.order_by('?')
    return JsonResponse(
        {
            'data': [
                {
                    'title': jingle.label,
                    'duration': '%d:%02d'
                    % (jingle.duration.total_seconds() / 60, jingle.duration.total_seconds() % 60),
                    'url': reverse('jingle-audio-view', kwargs={'pk': jingle.id}),
                }
                for jingle in jingles[:5]
            ]
        }
    )


def playing(request):
    try:
        latest_played = SomaLogLine.objects.select_related('track', 'track__artist').latest('play_timestamp')
    except SomaLogLine.DoesNotExist:
        return JsonResponse({'err': 1})
    duration = latest_played.track.duration
    elapsed = datetime.datetime.now() - latest_played.play_timestamp
    remaining = duration - elapsed
    if remaining.total_seconds() < 2:
        return JsonResponse({})
    elif remaining.total_seconds() < 0:
        remaining = datetime.timedelta(seconds=0)
    response = {}
    if latest_played.track.artist:
        response['artist'] = latest_played.track.artist.name
    response['title'] = latest_played.track.title
    response['duration'] = duration.seconds
    response['elapsed'] = int(elapsed.total_seconds())
    return JsonResponse(response)


def switch(request, *args, **kwargs):
    if not is_regie_computer(request):
        return HttpResponseForbidden()
    logger.info('regie switch to %s', request.GET['selection'])
    switch_audio_source(request.GET['selection'])
    return JsonResponse({'err': 0})
