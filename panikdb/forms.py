import django.contrib.auth.forms
from django import forms
from django.contrib.auth import authenticate, get_user_model


class AuthenticationForm(django.contrib.auth.forms.AuthenticationForm):
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = None
            if '@' in username:
                user = get_user_model().objects.filter(email=username).first()
                if user:
                    self.user_cache = authenticate(username=user.username, password=password)
            else:
                self.user_cache = authenticate(username=username, password=password)
            if self.user_cache is None:
                # raise self.get_invalid_login_error()  # 2.2.x
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data
