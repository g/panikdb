from django.urls import path

from . import views

urlpatterns = [
    path('authorize', views.authorize, name='oauth-authorize'),
    path('jwks', views.jwks, name='oauth-jwks'),
    path('token', views.token, name='oauth-token'),
    path('user-info', views.user_info, name='oauth-user-info'),
]
