import datetime

from django.core.management.base import BaseCommand
from django.utils.timezone import now

from panikdb.oauth.models import OAuthAccessToken, OAuthCode


class Command(BaseCommand):
    def handle(self, verbosity, **kwargs):
        OAuthCode.objects.filter(creation_timestamp__lt=now() - datetime.timedelta(days=1)).delete()
        OAuthAccessToken.objects.filter(creation_timestamp__lt=now() - datetime.timedelta(days=1)).delete()
