import base64
import datetime
import random

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden, HttpResponseNotAllowed, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.utils.text import slugify
from django.utils.timezone import now
from django.views.decorators.csrf import csrf_exempt
from jwcrypto.jwk import JWK
from jwcrypto.jwt import JWT

from .models import OAuthAccessToken, OAuthClient, OAuthCode, OAuthSub


@login_required
def authorize(request, *args, **kwargs):
    state = request.GET['state']
    client = OAuthClient.objects.get(client_id=request.GET['client_id'])
    code = OAuthCode.objects.create(client=client, user=request.user, nonce=request.GET.get('nonce'))
    redirect_uri = request.GET['redirect_uri']
    params = {
        'code': code.code,
        'state': state,
    }
    if request.GET.get('nonce'):
        params['nonce'] = request.GET.get('nonce')
    return HttpResponseRedirect(redirect_uri + '?%s' % '&'.join(['%s=%s' % x for x in params.items()]))


@csrf_exempt
def token(request, *args, **kwargs):
    if request.method != 'POST':
        return HttpResponseNotAllowed(['POST'])

    try:
        code = OAuthCode.objects.get(code=request.POST.get('code'))
    except OAuthCode.DoesNotExist:
        return HttpResponseForbidden()

    start = now()
    exp = start + datetime.timedelta(seconds=30)

    sub, created = OAuthSub.objects.get_or_create(client=code.client, user=code.user)

    id_token = {
        'iss': request.build_absolute_uri('/'),
        'aud': code.client.client_id,
        'sub': str(sub.sub),
        'exp': int(exp.timestamp()),
        'iat': int(start.timestamp()),
        'auth_time': int(start.timestamp()),
        'acr': '0',
    }
    if code.nonce:
        id_token['nonce'] = code.nonce
    header = {'alg': 'HS256'}
    k = base64.urlsafe_b64encode(code.client.client_secret.encode('utf-8')).strip(b'=')
    jwk = JWK(kty='oct', k=k.decode())
    jwt = JWT(header=header, claims=id_token)
    jwt.make_signed_token(jwk)

    access_token = OAuthAccessToken.objects.create(client=code.client, user=code.user)

    response = {
        'access_token': access_token.token,
        'token_type': 'Bearer',
        'expires_in': 30,
        'id_token': jwt.serialize(),
        'refresh_token': None,
    }

    return JsonResponse(response)


def user_info(request, *args, **kwargs):
    try:
        authorization = request.headers['authorization'].split()
    except KeyError:
        return HttpResponseForbidden('missing authorization header')
    if len(authorization) != 2 or authorization[0] != 'Bearer':
        return HttpResponseForbidden('invalid authorization header')
    try:
        access_token = OAuthAccessToken.objects.get(token=authorization[1])
    except OAuthAccessToken.DoesNotExist:
        return HttpResponseForbidden('missing access token')

    sub = OAuthSub.objects.get(client=access_token.client, user=access_token.user)

    user_info = {
        # use a random string as id, as it is required for pharum not to pick
        # an existing and wrong user.
        'id': str(random.randint(10**6, 10**7)),
        'email': access_token.user.email,
        'name': str(access_token.user),
        'username': slugify(str(access_token.user)),
        'sub': sub.sub,
    }
    return JsonResponse(user_info)


def jwks(request):
    return JsonResponse({'keys': []})


def well_known_openid_configuration(request):
    data = {
        'issuer': request.build_absolute_uri('/'),
        'authorization_endpoint': request.build_absolute_uri(reverse('oauth-authorize')),
        'token_endpoint': request.build_absolute_uri(reverse('oauth-token')),
        'jwks_uri': request.build_absolute_uri(reverse('oauth-jwks')),
        'userinfo_endpoint': request.build_absolute_uri(reverse('oauth-user-info')),
        'token_endpoint_auth_methods_supported': ['client_secret_post'],
    }
    return JsonResponse(data)
