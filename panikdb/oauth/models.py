import uuid

from django.conf import settings
from django.db import models


class OAuthClient(models.Model):
    client_id = models.CharField(max_length=100)
    client_secret = models.CharField(max_length=100, default=uuid.uuid4)

    def __str__(self):
        return self.client_id


class OAuthSub(models.Model):
    sub = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    client = models.ForeignKey(OAuthClient, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)


class OAuthCode(models.Model):
    code = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    client = models.ForeignKey(OAuthClient, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    nonce = models.CharField(max_length=200, null=True)


class OAuthAccessToken(models.Model):
    token = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    client = models.ForeignKey(OAuthClient, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
