# Django settings for panikdb project.

import copy
import os

from django.conf import global_settings

DEBUG = True
DEBUG_TOOLBAR = False

PROJECT_DIR = os.path.normpath(os.path.dirname(os.path.dirname(__file__)))
PROJECT_PATH = os.path.dirname(os.path.dirname(__file__))

ADMINS = (('Frederic Peters', 'fred@radiopanik.org'),)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_PATH, 'panikdb.sqlite3'),
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Brussels'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-be'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

LOCALE_PATHS = (os.path.join(PROJECT_DIR, 'panikdb', 'locale'),)

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_PATH, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(PROJECT_PATH, 'static')

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

CKEDITOR_UPLOAD_PATH = 'uploads/'
CKEDITOR_UPLOAD_PREFIX = '/media/uploads/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_PATH, 'panikdb', 'static'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = list(global_settings.STATICFILES_FINDERS) + ['gadjo.finders.XStaticFinder']

# Make this unique, and don't share it with anybody.
SECRET_KEY = '(jwx-1y#d#vps93glikirr&tq_!^_4g+9-qj(jy#l=sllqw(^j'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(PROJECT_PATH, 'panikdb', 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'panikdb.context_processors.internal_ip',
                'panikdb.context_processors.site_settings',
            ],
            'builtins': [
                'combo.public.templatetags.combo',
            ],
        },
    },
]


MIDDLEWARE = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'panikdb.middleware.StoreRequestMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'panikdb.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'panikdb.wsgi.application'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'ckeditor',
    'taggit',
    'emissions',
    'newsletter',
    'matos',
    'wiki',
    'agendas',
    'nonstop',
    'panikdb.aa',
    'panikdb.customtags',
    'panikdb.mastodon',
    'panikdb.service_messages',
    'panikdb.oauth',
    'panikdb.pige',
    'panikdb.regie',
    'panikdb.stats',
    'panikdb.poll',
    'gadjo',
    'combo.data',
    'combo.locales',
    'combo.profile',
    'combo.public',
    'combo.manager',
    'combo.apps.assets',
    'combo.apps.dashboard',
    'combo.apps.export_import',
    'combo.apps.gallery',
    'combo.apps.kb',
    'combo.apps.maps',
    'combo.apps.notifications',
    'combo.apps.pwa',
    'combo.apps.search',
    'publik_django_templatetags.publik',
    'sorl.thumbnail',
    'panikombo',
    'xstatic.pkg.select2',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {'require_debug_false': {'()': 'django.utils.log.RequireDebugFalse'}},
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    },
}

CKEDITOR_CONFIGS = {
    'default': {
        'extraAllowedContent': 'u i b strong em small',
        'disallowedContent': 'style; script; *[on*]',
        'filebrowserUploadUrl': '/ckeditor/upload/',
        'filebrowserBrowseUrl': '/ckeditor/browse/',
        'toolbar_Own': [
            ['Source', 'Format', '-', 'Bold', 'Italic'],
            ['NumberedList', 'BulletedList'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink'],
            [
                'Image',
            ],
            [
                'RemoveFormat',
            ],
        ],
        'toolbar': 'Own',
    },
}

CKEDITOR_CONFIGS['small'] = copy.copy(CKEDITOR_CONFIGS['default'])
CKEDITOR_CONFIGS['small']['height'] = 150

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    },
}

TAGGIT_TAGS_FROM_STRING = 'emissions.utils.custom_parse_tags'

AUTH_USER_MODEL = 'aa.User'
MEMBERSHIP_DEFAULT_AMOUNT = 10

LOGIN_REDIRECT_URL = '/'
WEBSITE_BASE_URL = 'https://www.radiopanik.org/'

NEWSLETTER_SENDER = 'info@radiopanik.org'
NEWSLETTER_DOMAIN = 'radiopanik.org'
NEWSLETTER_STYLE = """
body { color: #222; text-align: justify; }
h2, h3 { font-family: "Reglo"; }
h3 { margin-bottom: 0; }
h3 + p { margin-top: 0; }
p strong { color: black; }
h3 strong { text-transform: uppercase; color: blue; }
p a { color: black; text-decoration: none; border-bottom: 1px dotted black; }
p a:hover { border-bottom: 1px solid black; }
"""

RAVEN_CONFIG = None

from panikombo.misc import COMBO_CELL_TEMPLATES, COMBO_PUBLIC_TEMPLATES

COMBO_ASSET_SLOTS = {}
COMBO_DASHBOARD_ENABLED = False
COMBO_DEFAULT_PUBLIC_TEMPLATE = 'standard'
COMBO_MAP_TILE_URLTEMPLATE = ''
COMBO_MAP_ATTRIBUTION = ''
COMBO_MANAGE_HOME_COLLAPSE_PAGES = False
COMBO_SEARCH_SERVICES = {}
COMBO_PUBLIC_TEMPLATES_ROOT_PAGE = 'modeles'
POSTGRESQL_FTS_SEARCH_CONFIG = 'french_unaccent'
REQUESTS_TIMEOUT = 28
COMBO_ASSET_IMAGE_MAX_WIDTH = 2000

AUTO_RENDER_SELECT2_STATICS = False

REGIE_IPS = ['192.168.17.121', '192.168.17.120', '127.0.0.1']

# list of dicts, ex:
# PIGES = [
#    {
#        'slug': 'pige',
#        'name': 'Pige',
#        'base_dir': '/home/fred/src/panik/sample-pige/srv/pige/',
#    },
#    ...
PIGES = []

JSON_CELL_TYPES = {}
TEMPLATE_VARS = {
    'pwa_display': 'standalone',
    'theme_color': "#000000",
}

HAYSTACK_CONNECTIONS = {'default': {}}

FILE_UPLOAD_DIRECTORY_PERMISSIONS = 0o755
FILE_UPLOAD_PERMISSIONS = 0o644

HAS_SOMA = False
HAS_CMS = True
HAS_NONSTOP = True
HAS_WIKI = True
HAS_REGIE = True
HAS_PIGE = True
HAS_NEWSLETTERS = True
HAS_RECORDING_BUTTONS = False
HAS_MEMBERSHIP_SUPPORT = False
HAS_MATOS = False
USE_EXTERNAL_SOUNDS = False
USE_AGENDA_ONLY_FIELD = False
HAS_EMISSION_CHATS = False

MEMBERSHIP_HELP_URL = None

SITE_TITLE = 'PanikDB'
RADIO_NAME = 'Radio Panik'
COMBO_CELL_ASSET_SLOTS = {}
WIKI_SEARCH_BASE_URL = None
CELL_CONDITIONS_ENABLED = False
FTS_DICTIONARY_CONFIG = 'french'
NONSTOP_AUTO_SCHEDULE = True
KNOWN_SERVICES = {}

HOME_EXTRA_TOP_LINKS = []

# rsync base location for upload to website
# ex: panik@xxx:/srv/www.radiopanik.org/panikweb/media
WEBSITE_MEDIA_SOUNDS_SYNC_BASE = None

WEBSITE_EMISSIONS_PREFIX = 'emissions/'
WEBSITE_NEWSITEMS_PREFIX = 'actus/'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

local_settings_file = os.environ.get(
    'PANIKDB_SETTINGS_FILE', os.path.join(os.path.dirname(__file__), 'local_settings.py')
)
if os.path.exists(local_settings_file):
    exec(open(local_settings_file).read())


if DEBUG and DEBUG_TOOLBAR:
    MIDDLEWARE += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
    INSTALLED_APPS += ('debug_toolbar',)

if RAVEN_CONFIG:
    INSTALLED_APPS += ('raven.contrib.django.raven_compat',)
