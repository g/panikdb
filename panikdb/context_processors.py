from combo.apps.pwa.models import PwaSettings
from combo.utils.cache import cache_during_request
from django.conf import settings


def internal_ip(request):
    current_ip = (
        request.headers.get('x-forwarded-for')
        or request.headers.get('x-real-ip')
        or request.META.get('REMOTE_ADDR')
    )
    return {
        'internal_ip': current_ip in settings.INTERNAL_IPS,
        'current_ip': current_ip,
    }


def site_settings(request):
    d = {
        'site_title': settings.SITE_TITLE,
        'has_soma': settings.HAS_SOMA,
        'has_pige_download': bool(settings.PIGES),
        'has_matos': settings.HAS_MATOS,
        'has_wiki': settings.HAS_WIKI,
        'has_cms': settings.HAS_CMS,
        'has_nonstop': settings.HAS_NONSTOP,
        'has_regie': settings.HAS_REGIE,
        'has_recording_buttons': settings.HAS_RECORDING_BUTTONS,
        'has_membership_support': settings.HAS_MEMBERSHIP_SUPPORT,
        'has_newsletters': settings.HAS_NEWSLETTERS,
        'has_auto_schedule': getattr(settings, 'NONSTOP_AUTO_SCHEDULE', False),
        'pwa_settings': cache_during_request(PwaSettings.singleton),
        'piges': settings.PIGES,
        'wiki_search_base_url': settings.WIKI_SEARCH_BASE_URL,
        'website_url': settings.WEBSITE_BASE_URL.strip('/'),
    }
    d.update(settings.TEMPLATE_VARS)
    return d
