from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


class Vote(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    choice = models.CharField(_('Choice'), max_length=10)
    comment = models.TextField(_('Comment'))
    creation_timestamp = models.DateTimeField(auto_now_add=True)
