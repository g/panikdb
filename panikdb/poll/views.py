from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView

from .models import Vote


class VoteView(TemplateView):
    template_name = 'vote.html'

    def dispatch(self, request, *args, **kwargs):
        return redirect('home')
        try:
            vote = Vote.objects.get(user=request.user)
            messages.error(request, 'Non !')
            return redirect('home')
        except Vote.DoesNotExist:
            pass
        return super().dispatch(request, *args, **kwargs)

    def post(self, request):
        choice = request.POST['choice']
        comment = request.POST['comment']

        vote = Vote(user=request.user, choice=request.POST['choice'], comment=request.POST['comment'])
        vote.save()
        messages.success(request, 'Merci !')
        return redirect('home')


vote = login_required(VoteView.as_view())


class ResultsView(ListView):
    model = Vote
    template_name = 'vote_results.html'

    def get_queryset(self):
        return Vote.objects.order_by('?')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['choice_1'] = self.get_queryset().filter(choice="1").count()
        context['choice_2'] = self.get_queryset().filter(choice="2").count()
        context['choice_3'] = self.get_queryset().filter(choice="3").count()
        return context


vote_results = login_required(ResultsView.as_view())
