import logging

from .middleware import StoreRequestMiddleware


class RequestContextFilter(logging.Filter):
    def filter(self, record):
        if not hasattr(record, 'request'):
            record.request = StoreRequestMiddleware.get_request()
        request = record.request

        if not hasattr(record, 'user'):
            if (
                hasattr(request, 'user')
                and hasattr(request.user, 'is_authenticated')
                and request.user.is_authenticated
            ):
                record.user = request.user
            else:
                record.user = None

        if record.user:
            record.username = record.user.username or record.user.email
        else:
            record.username = '-'

        return True
