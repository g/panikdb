import datetime
import json

import requests
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from panikdb.stats.models import DailyStat, SoundFile


class Command(BaseCommand):
    def handle(self, verbosity, **kwargs):
        self.verbose = verbosity > 1

        piwik_api_href = settings.PIWIK_API_HREF
        piwik_token_auth = settings.PIWIK_TOKEN_AUTH
        piwik_site_id = settings.PIWIK_SITE_ID
        url = (
            '%(piwik_api_href)s?module=API&method=Events.getAction&'
            'secondaryDimension=eventName&idSite=%(piwik_site_id)s&'
            'period=day&date=last2&format=json&'
            'token_auth=%(piwik_token_auth)s&expanded=1' % locals()
        )
        if self.verbose:
            print('calling piwik')
        response = requests.get(url, headers={'Accept': 'application/json'})
        if not response.ok:
            print(f'invalid response (status: {response.status_code})')
            raise CommandError('invalid response')
        result = response.json()
        if self.verbose:
            print('collecting results')
        for day in result:
            day_datetime = datetime.datetime.strptime(day, '%Y-%m-%d')
            for stat_segment in result.get(day):
                if stat_segment.get('segment') != 'eventAction==Play':
                    continue
                for entry in stat_segment.get('subtable'):
                    sound_id = entry.get('label').split(':')[0]
                    try:
                        soundfile = SoundFile.objects.get(id=sound_id)
                    except (SoundFile.DoesNotExist, ValueError):
                        if self.verbose:
                            print('failed to process', entry.get('label'))
                        continue
                    try:
                        stat = DailyStat.objects.get(soundfile=soundfile, day=day_datetime)
                    except DailyStat.DoesNotExist:
                        stat = DailyStat(soundfile=soundfile, day=day_datetime)
                    for attribute in ('nb_events', 'nb_visits'):
                        setattr(stat, attribute, entry.get(attribute))
                    stat.save()
