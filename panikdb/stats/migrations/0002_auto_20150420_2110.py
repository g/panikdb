import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('stats', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailystat',
            name='soundfile',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.SET_NULL, to='emissions.SoundFile', null=True
            ),
            preserve_default=True,
        ),
    ]
