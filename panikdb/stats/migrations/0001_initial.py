from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0003_newsitem_event_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='DailyStat',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('day', models.DateField()),
                ('nb_events', models.IntegerField()),
                ('nb_visits', models.IntegerField()),
                ('soundfile', models.ForeignKey(to='emissions.SoundFile', on_delete=models.SET_NULL)),
            ],
            options={},
            bases=(models.Model,),
        ),
    ]
