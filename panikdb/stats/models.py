from django.db import models
from emissions.models import SoundFile


class DailyStat(models.Model):
    soundfile = models.ForeignKey(SoundFile, on_delete=models.SET_NULL, null=True)
    day = models.DateField()
    nb_events = models.IntegerField()
    nb_visits = models.IntegerField()
