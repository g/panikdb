from django.db import models
from emissions.models import Episode, NewsItem


class MastodonSettings(models.Model):
    base_url = models.CharField(max_length=255)
    client_id = models.CharField(max_length=255)
    client_secret = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)

    @classmethod
    def singleton(cls):
        return cls.objects.all().first() or cls()

    def configured(self):
        return bool(
            self.base_url and self.client_id and self.client_secret and self.username and self.password
        )


class MastodonEntry(models.Model):
    episode = models.ForeignKey(Episode, on_delete=models.CASCADE, null=True)
    newsitem = models.ForeignKey(NewsItem, on_delete=models.CASCADE, null=True)
    creation_timestamp = models.DateTimeField(auto_now_add=True)

    status_id = models.CharField(max_length=255, null=True)
