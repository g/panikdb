import datetime
import logging

from django.core.management.base import BaseCommand, CommandError
from emissions.models import Episode, NewsItem
from mastodon import Mastodon

from panikdb.mastodon.models import MastodonEntry, MastodonSettings

logger = logging.getLogger('panikdb')


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--register-app',
            dest='register_base_url',
            metavar='REGISTER_BASE_URL',
            help='Register application onto server at BASE_URL',
        )
        parser.add_argument(
            '--set-username',
            dest='register_username',
            metavar='USERNAME',
            help='Register username',
        )
        parser.add_argument(
            '--set-password',
            dest='register_password',
            metavar='PASSWORD',
            help='Register password',
        )
        parser.add_argument(
            '--simulate',
            action='store_true',
            help='Simulate, do not post for real',
        )
        parser.add_argument(
            '--single',
            action='store_true',
            help='Add a single toot then stop',
        )

    def handle(self, verbosity, **kwargs):
        mastodon_settings = MastodonSettings.singleton()
        if kwargs.get('register_base_url') and not kwargs.get('simulate'):
            client_id, client_secret = Mastodon.create_app(
                'panikdb', api_base_url=kwargs.get('register_base_url')
            )
            mastodon_settings.base_url = kwargs.get('register_base_url')
            mastodon_settings.client_id = client_id
            mastodon_settings.client_secret = client_secret
            mastodon_settings.save()

        if kwargs.get('register_username'):
            mastodon_settings.username = kwargs.get('register_username')
            mastodon_settings.save()

        if kwargs.get('register_password'):
            mastodon_settings.password = kwargs.get('register_password')
            mastodon_settings.save()

        if (
            kwargs.get('register_base_url')
            or kwargs.get('register_username')
            or kwargs.get('register_password')
        ):
            return

        if not (kwargs.get('simulate') or mastodon_settings.configured()):
            # exit quietly
            return

        if not kwargs.get('simulate'):
            mastodon = Mastodon(
                client_id=mastodon_settings.client_id,
                client_secret=mastodon_settings.client_secret,
                api_base_url=mastodon_settings.base_url,
            )
            mastodon.log_in(mastodon_settings.username, mastodon_settings.password)

        episodes = (
            Episode.objects.filter(
                soundfile__fragment=False,
                soundfile__podcastable=True,
                soundfile__last_update_timestamp__lt=datetime.datetime.now() - datetime.timedelta(minutes=30),
            )
            .exclude(title__icontains='redif')
            .exclude(subtitle__icontains='redif')
            .exclude(first_diffusion_date__isnull=True)
            .filter(first_diffusion_date__lt=datetime.datetime.now())
            .filter(first_diffusion_date__gt=datetime.datetime.now() - datetime.timedelta(days=3))
            .order_by('-first_diffusion_date')
            .distinct()
        )

        for episode in episodes:
            entry, created = MastodonEntry.objects.get_or_create(episode=episode)
            if entry.status_id:
                continue
            parts = []
            if episode.emission.title in episode.title:
                parts.append(f'🎧 {episode.title}')
            else:
                parts.append(f'🎧 {episode.emission.title} - {episode.title}')
            if episode.subtitle:
                parts.append(episode.subtitle)
            parts.append(episode.get_site_url())
            message = '\n'.join(parts)
            if verbosity > 1:
                print(f'tooting episode: {episode.title} ({episode.emission.title})')
            if kwargs.get('simulate'):
                print('[simulating], toot:')
                print(message)
            else:
                logger.info(f'tooting episode: {episode.title} ({episode.emission.title})')
                status_dict = mastodon.status_post(message, idempotency_key=episode.get_site_url())
                entry.status_id = status_dict.get('id')
                entry.save()
            if kwargs.get('single'):
                break

        newsitems = NewsItem.objects.filter(
            creation_timestamp__gt=datetime.datetime.now() - datetime.timedelta(minutes=120),
            emission__isnull=True,
        )

        for newsitem in newsitems:
            entry, created = MastodonEntry.objects.get_or_create(newsitem=newsitem)
            if entry.status_id:
                continue
            parts = []
            parts.append(f'{newsitem.title}')
            if newsitem.subtitle:
                parts.append(newsitem.subtitle)
            parts.append(newsitem.get_site_url())
            message = '\n'.join(parts)
            if verbosity > 1:
                print(f'tooting newsitem: {newsitem.title}')
            if kwargs.get('simulate'):
                print('[simulating], toot:')
                print(message)
            else:
                logger.info(f'tooting newsitem: {newsitem.title}')
                status_dict = mastodon.status_post(message, idempotency_key=newsitem.get_site_url())
                entry.status_id = status_dict.get('id')
                entry.save()
            if kwargs.get('single'):
                break
