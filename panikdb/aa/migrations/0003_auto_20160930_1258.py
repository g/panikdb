from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('aa', '0002_auto_20150125_1431'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='user',
            options={'ordering': ['first_name', 'last_name']},
        ),
        migrations.AddField(
            model_name='user',
            name='mobile',
            field=models.CharField(max_length=20, null=True, verbose_name='Mobile', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='user',
            name='phone',
            field=models.CharField(max_length=20, null=True, verbose_name='Phone', blank=True),
            preserve_default=True,
        ),
    ]
