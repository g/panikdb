from django.urls import path

from . import views

urlpatterns = [
    path('members/', views.members_list_view, name='members-list-view'),
    path('members/new/', views.member_new, name='member-new'),
    path('members/vcard/', views.members_vcard, name='members-vcard'),
    path('members/csv/', views.members_csv, name='members-csv'),
    path('members/<int:pk>/', views.member_view, name='member-view'),
    path('members/<int:pk>/edit/', views.member_edit, name='member-edit'),
    path('members/<int:pk>/emissions/', views.member_emissions, name='member-emissions'),
    path(
        'members/<int:pk>/emissions/remove/<int:em_pk>/',
        views.member_emission_remove,
        name='member-emission-remove',
    ),
    path('members/<int:pk>/mark-as-active/', views.mark_as_active, name='member-mark-as-active'),
    path('members/<int:pk>/mark-as-inactive/', views.mark_as_inactive, name='member-mark-as-inactive'),
    path('members/<int:pk>/password/', views.member_password, name='member-password'),
    path(
        'members/<int:pk>/register-membership/',
        views.register_membership,
        name='member-register-membership',
    ),
    path('members/groups/', views.groups_list, name='groups-list'),
    path('members/groups/new/', views.group_new, name='group-new'),
    path('members/groups/<int:pk>/', views.group_view, name='group-view'),
    path('members/groups/<int:pk>/perms/', views.group_update_perms, name='group-update-perms'),
    path('members/groups/<int:pk>/delete/', views.group_delete, name='group-delete'),
    path('profile/', views.profile_view, name='profile-view'),
    path('profile/edit/', views.profile_contact_edit, name='profile-contact-edit'),
]
