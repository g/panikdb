from django import forms
from django.core.exceptions import ValidationError
from django.utils.crypto import get_random_string
from django.utils.translation import gettext_lazy as _

from .models import Membership, User


class MemberEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'phone', 'mobile', 'groups']
        widgets = {
            'groups': forms.CheckboxSelectMultiple(),
        }

    def __init__(self, *args, hide_agenda=False, category=None, booking=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groups'].queryset = self.fields['groups'].queryset.order_by('name')
        self.fields['groups'].help_text = None

    def clean_email(self):
        email = self.cleaned_data['email']
        if email and User.objects.exclude(pk=self.instance.id).filter(email=email).exists():
            raise ValidationError(_('This email is already used.'))
        return email


class MemberCreateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password']
        widgets = {'password': forms.PasswordInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['email'].required = True
        self.fields['password'].required = False
        self.fields['password'].help_text = _('Can be left empty to get a random password generated.')

    def clean_email(self):
        email = self.cleaned_data['email']
        if email and User.objects.filter(email=email).exists():
            raise ValidationError(_('This email is already used.'))
        return email

    def clean_password(self):
        password = self.cleaned_data['password']
        if not password:
            password = get_random_string(length=12)
        return password


def get_emissions_as_choices():
    from emissions.models import Emission

    return [(None, '')] + [(x.id, x.title) for x in Emission.objects.exclude(archived=True)]


class MemberEmissionForm(forms.Form):
    emission = forms.ChoiceField(label=_('New Emission'), required=True, choices=get_emissions_as_choices)


class MembershipForm(forms.ModelForm):
    class Meta:
        model = Membership
        fields = ['year', 'payment_date', 'payment_amount', 'notes']
