import csv
import io

import vobject
from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.contrib.auth.models import Group, Permission
from django.core.exceptions import PermissionDenied
from django.db.models import Q, Sum
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.views.decorators.http import require_POST
from django.views.generic.base import RedirectView, TemplateView, View
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, FormView, UpdateView
from django.views.generic.list import ListView

from .forms import MemberCreateForm, MemberEditForm, MemberEmissionForm, MembershipForm
from .models import Membership, User

RELEVANT_PERMISSIONS = [
    ('nonstop.add_track', _('Add tracks to nonstop')),
    ('emissions.change_nonstop', _('Adjust nonstop schedule')),
    ('emissions.change_schedule', _('Adjust emissions schedules')),
    ('emissions.add_diffusion', _('Add a diffusion out of regular schedules')),
    ('emissions.add_emission', _('Add a new emission')),
    ('emissions.add_newsitem', _('Add a new newsitem')),
    ('emissions.delete_emission', _('Delete an emission (careful!)')),
    ('nonstop.add_scheduleddiffusion', _('Define a stream in an episode')),
    ('nonstop.change_stream', _('Manage available streams')),
    ('service_messages.change_message', _('Manage service messages')),
    ('agendas.add_booking', _('Manage agendas')),
    ('aa.add_user', _('Manage users and groups')),
    ('matos.add_piece', _('Manage hardware inventory')),
    ('matos.add_loan', _('Manage hardware loans')),
]


class ProfileView(TemplateView):
    template_name = 'aa/profile.html'


profile_view = login_required(ProfileView.as_view())


class ProfileContactEditView(UpdateView):
    model = User
    fields = ['phone', 'mobile', 'share_contact_details']
    success_url = reverse_lazy('profile-view')
    template_name = 'aa/profile_form.html'

    def get_object(self):
        return self.request.user


profile_contact_edit = login_required(ProfileContactEditView.as_view())


class MembersListView(ListView):
    paginate_by = 10

    def get_queryset(self):
        qs = User.objects.all().prefetch_related('emissions').prefetch_related('groups')
        if not (self.request.user.is_staff and self.request.GET.get('q')):
            # filter on active users unless a search term is given
            qs = qs.filter(is_active=True)
        if self.request.GET.get('q'):
            for part in self.request.GET.get('q').split():
                part = part.strip()
                if not part:
                    continue
                if part.lower() in ('ca', 'cp'):
                    qs = qs.filter(groups__name=part.upper())
                    continue
                qs = qs.filter(
                    Q(first_name__icontains=part)
                    | Q(last_name__icontains=part)
                    | Q(emissions__title__icontains=part)
                    | Q(emissions__slug__icontains=part)
                )
        current_year = now().year
        if self.request.GET.get('membership') == 'ok':
            qs = qs.filter(membership__year=current_year)
        if self.request.GET.get('membership') == 'renew':
            qs = qs.filter(membership__year=current_year - 1).exclude(membership__year=current_year)
        qs = qs.distinct()
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.GET.get('membership') == 'ok':
            context['total_membership'] = True
            context['total_money'] = (
                self.get_queryset()
                .aggregate(Sum('membership__payment_amount'))
                .get('membership__payment_amount__sum')
            )
        return context


members_list_view = login_required(MembersListView.as_view())


class MembersVCardView(MembersListView):
    def get_queryset(self):
        qs = super().get_queryset()
        if not self.request.user.is_staff:
            qs = qs.filter(share_contact_details=True)
        qs = qs.exclude(email='')
        return qs

    def render_to_response(self, context, **response_kwargs):
        members = []
        for member in self.get_queryset():
            card = vobject.vCard()
            card.add('n')
            card.n.value = vobject.vcard.Name(family=member.last_name, given=member.first_name)
            card.add('fn')
            card.fn.value = str(member)
            card.add('email')
            card.email.value = member.email
            card.email.type_param = 'INTERNET'
            card.add('note')
            note_parts = []
            if member.is_ca():
                note_parts.append('CA')
            if member.is_cp():
                note_parts.append('CP')
            note_parts.extend([str(x) for x in member.active_emissions()])
            card.note.value = ', '.join(note_parts)
            if member.phone:
                card.add('tel')
                card.tel.value = member.phone
                card.tel.type_param = 'HOME'
            if member.mobile:
                card.add('tel')
                card.tel.value = member.mobile
                card.tel.type_param = 'CELL'
            members.append(card.serialize())
        content = ''.join(members)
        return HttpResponse(content, content_type='text/vcard')


members_vcard = login_required(MembersVCardView.as_view())


class MembersCsvView(MembersVCardView):
    def render_to_response(self, context, **response_kwargs):
        output = io.StringIO()
        csv_output = csv.writer(output, quoting=csv.QUOTE_ALL)
        csv_output.writerow(
            [
                _('First name'),
                _('Last name'),
                _('Email'),
                _('Phone'),
                _('Note'),
                _('%(year)s membership') % {'year': now().year},
                _('Emissions'),
            ]
        )
        for member in self.get_queryset():
            csv_output.writerow(
                [
                    member.first_name or '',
                    member.last_name or '',
                    member.email or '',
                    f'{member.phone} - {member.mobile}'
                    if (member.phone and member.mobile)
                    else (member.phone or member.mobile or ''),
                    _('inactive')
                    if not member.is_active
                    else 'CA'
                    if member.is_ca()
                    else 'CP'
                    if member.is_cp()
                    else '',
                    _('yes') if member.has_current_membership() else _('no'),
                    ' / '.join([x.title for x in member.active_emissions()]),
                ]
            )
        return HttpResponse(
            output.getvalue(),
            content_type='text/csv; charset=utf-8',
            headers={'content_disposition': 'attachment; filename=members.csv'},
        )


members_csv = login_required(MembersCsvView.as_view())


class MemberView(DetailView):
    model = User

    def get_queryset(self):
        return User.objects.all().prefetch_related('emissions').prefetch_related('groups')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_year = now().year
        user = self.get_object()
        memberships = user.membership_set.all().order_by('-year')
        try:
            context['current_membership'] = [x for x in memberships if x.year == current_year][0]
        except IndexError:
            pass
        context['past_memberships'] = [x for x in memberships if x.year != current_year]
        return context


member_view = login_required(MemberView.as_view())


class MemberEditView(UpdateView):
    model = User
    form_class = MemberEditForm

    def get_object(self):
        if not self.request.user.has_perm('aa.add_user'):
            raise PermissionDenied()
        return super().get_object()

    def form_valid(self, form):
        response = super().form_valid(form)
        if Group.objects.filter(name__iexact='admin').exists():
            self.object.is_superuser = bool(self.object.groups.filter(name__iexact='admin').exists())
            self.object.save()
        return response

    def get_success_url(self):
        return reverse_lazy('member-view', kwargs={'pk': self.get_object().id})


member_edit = login_required(MemberEditView.as_view())


class MemberPasswordView(FormView):
    form_class = AdminPasswordChangeForm
    template_name = 'aa/member_password.html'

    def get_object(self):
        if not self.request.user.has_perm('aa.add_user'):
            raise PermissionDenied()
        return User.objects.get(id=self.kwargs['pk'])

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.get_object()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_pk'] = self.kwargs['pk']
        return context

    def form_valid(self, form):
        response = super().form_valid(form)
        form.save()
        return response

    def get_success_url(self):
        return reverse_lazy('member-view', kwargs={'pk': self.kwargs['pk']})


member_password = login_required(MemberPasswordView.as_view())


class MemberCreateView(CreateView):
    model = User
    form_class = MemberCreateForm

    def get_object(self):
        if not self.request.user.has_perm('aa.add_user'):
            raise PermissionDenied()
        return super().get_object()

    def form_valid(self, form):
        form.instance.username = form.instance.email
        response = super().form_valid(form)
        self.object.set_password(form.cleaned_data['password'])
        self.object.save()
        return response

    def get_success_url(self):
        return reverse_lazy('member-view', kwargs={'pk': self.object.id})


member_new = login_required(MemberCreateView.as_view())


class MemberEmissionsView(FormView):
    form_class = MemberEmissionForm
    template_name = 'aa/member_emissions.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['member'] = User.objects.get(id=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        response = super().form_valid(form)
        member = User.objects.get(id=self.kwargs['pk'])
        member.emissions.add(form.cleaned_data['emission'])
        return response

    def get_success_url(self):
        return reverse_lazy('member-view', kwargs={'pk': self.kwargs['pk']})


member_emissions = login_required(MemberEmissionsView.as_view())


class MemberEmissionRemoveView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if not self.request.user.has_perm('aa.add_user'):
            raise PermissionDenied()
        member = User.objects.get(id=kwargs['pk'])
        member.emissions.remove(kwargs['em_pk'])
        return reverse_lazy('member-view', kwargs={'pk': member.id})


member_emission_remove = login_required(MemberEmissionRemoveView.as_view())


@staff_member_required
def mark_as_active(request, pk):
    User.objects.filter(pk=pk).update(is_active=True)
    return HttpResponseRedirect(reverse_lazy('member-view', kwargs={'pk': pk}))


@staff_member_required
def mark_as_inactive(request, pk):
    User.objects.filter(pk=pk).update(is_active=False)
    return HttpResponseRedirect(reverse_lazy('member-view', kwargs={'pk': pk}))


class RegisterMembershipView(FormView):
    form_class = MembershipForm
    template_name = 'aa/register_membership.html'

    def get_context_data(self, **kwargs):
        if not self.request.user.has_perm('aa.add_membership'):
            raise PermissionDenied()
        context = super().get_context_data(**kwargs)
        context['member'] = User.objects.get(id=self.kwargs['pk'])
        return context

    def get_initial(self):
        initial = super().get_initial()
        initial['year'] = now().year
        initial['payment_date'] = now().date()
        initial['payment_amount'] = settings.MEMBERSHIP_DEFAULT_AMOUNT or ''
        return initial

    def form_valid(self, form):
        member = User.objects.get(id=self.kwargs['pk'])
        membership, created = Membership.objects.get_or_create(member=member, year=form.cleaned_data['year'])
        membership.payment_date = form.cleaned_data['payment_date']
        membership.payment_amount = form.cleaned_data['payment_amount']
        membership.notes = form.cleaned_data['notes']
        membership.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('member-view', kwargs={'pk': self.kwargs['pk']})


register_membership = login_required(RegisterMembershipView.as_view())


class GroupsList(ListView):
    paginate_by = 100
    template_name = 'aa/groups_list.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.has_perm('aa.add_user'):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return Group.objects.order_by('name')


groups_list = login_required(GroupsList.as_view())


class GroupCreateView(CreateView):
    model = Group
    fields = ['name']
    template_name = 'aa/group_new.html'
    success_url = reverse_lazy('groups-list')

    def get_object(self):
        if not self.request.user.has_perm('aa.add_user'):
            raise PermissionDenied()
        return super().get_object()


group_new = login_required(GroupCreateView.as_view())


class GroupDetailView(DetailView):
    model = Group
    template_name = 'aa/group_detail.html'

    def get_object(self):
        if not self.request.user.has_perm('aa.add_user'):
            raise PermissionDenied()
        return super().get_object()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        group = self.get_object()
        has_perms = [
            f'{x.content_type.app_label}.{x.codename}' for x in group.permissions.all().select_related()
        ]
        context['perms'] = [
            {'code': x[0], 'label': x[1], 'enabled': x[0] in has_perms} for x in RELEVANT_PERMISSIONS
        ]
        return context


group_view = login_required(GroupDetailView.as_view())


class GroupUpdatePerms(View):
    def post(self, request, *args, **kwargs):
        if not request.user.has_perm('aa.add_user'):
            raise PermissionDenied()
        group = Group.objects.get(**kwargs)
        set_permissions = set()
        unset_permissions = set()
        for perm in RELEVANT_PERMISSIONS:
            app_label, codename = perm[0].split('.')
            perm_obj = Permission.objects.filter(content_type__app_label=app_label, codename=codename).first()
            if not perm_obj:
                continue
            if perm[0] in request.POST.getlist('perms'):
                group.permissions.add(perm_obj)
            else:
                group.permissions.remove(perm_obj)
        return HttpResponseRedirect(reverse_lazy('groups-list'))


group_update_perms = login_required(require_POST(GroupUpdatePerms.as_view()))


class GroupDeleteView(DeleteView):
    model = Group
    template_name = 'aa/group_confirm_delete.html'
    success_url = reverse_lazy('groups-list')

    def get_object(self):
        if not self.request.user.has_perm('aa.add_user'):
            raise PermissionDenied()
        return super().get_object()


group_delete = login_required(GroupDeleteView.as_view())
