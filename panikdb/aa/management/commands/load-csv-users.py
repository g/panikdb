import csv
from optparse import make_option

from django.core.management.base import BaseCommand, CommandError
from django.utils.text import slugify

from ...models import Emission, User


class Command(BaseCommand):
    args = 'filename'

    def handle(self, filename, verbosity, **kwargs):
        self.verbose = verbosity > 1
        for line in csv.reader(file(filename)):
            email, lastname, firstname = (unicode(x.strip(), 'utf-8') for x in line[:3])
            emission_slugs = [x.strip().lower() for x in line[4:9] if x]
            if not email or not emission_slugs:
                continue
            if firstname and lastname:
                username = slugify('%s-%s' % (firstname, lastname))
            elif firstname or lastname:
                username = slugify(firstname or lastname)
                if len(username) < 8:
                    username = '%s.%s' % (username, slugify(unicode(emission_slugs[0])))
            else:
                continue

            username = username[:28]

            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                user = User.objects.create_user(username, email)
                user.set_password('panik')
            user.first_name = firstname
            user.last_name = lastname
            emissions = []
            for slug in emission_slugs:
                try:
                    emissions.append(Emission.objects.get(slug=slug))
                except Emission.DoesNotExist:
                    print('E: missing emission:', slug)
            user.emissions = emissions
            user.save()
