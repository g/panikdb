import re

from django.contrib.auth.models import AbstractUser
from django.core import validators
from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from emissions.models import Emission, Episode, NewsCategory, NewsItem, SoundFile


class User(AbstractUser):
    emissions = models.ManyToManyField(Emission, blank=True)
    news_categories = models.ManyToManyField(NewsCategory, blank=True)

    phone = models.CharField(_('Phone'), max_length=20, null=True, blank=True)
    mobile = models.CharField(_('Mobile'), max_length=20, null=True, blank=True)
    share_contact_details = models.BooleanField(_('Share contact detais with members'), default=True)

    class Meta:
        ordering = ['first_name', 'last_name']

    def can_manage(self, object):
        if self.is_staff:
            return True
        if isinstance(object, Emission):
            return self.has_perm('emissions.change_emission') or object in self.emissions.all()
        if isinstance(object, Episode):
            return self.has_perm('emissions.change_episode') or object.emission in self.emissions.all()
        if isinstance(object, SoundFile):
            return (
                self.has_perm('emissions.change_soundfile') or object.episode.emission in self.emissions.all()
            )
        if isinstance(object, NewsItem):
            return self.has_perm('emissions.change_newsitem') or object.emission in self.emissions.all()
        return False

    def active_emissions(self):
        # list comprehension to act on prefetched emissions
        return [x for x in self.emissions.all() if not x.archived]

    def is_ca(self):
        return bool(any(x for x in self.groups.all() if x.name == 'CA'))

    def is_cp(self):
        return bool(any(x for x in self.groups.all() if x.name == 'CP'))

    def has_current_membership(self):
        current_year = now().year
        return self.membership_set.filter(year=current_year).exists()

    def __str__(self):
        s = super().__str__()
        parts = []
        if self.first_name:
            parts.append(self.first_name)
        if self.last_name:
            parts.append(self.last_name)
        if parts:
            s = ' '.join(parts)
        return s


class Membership(models.Model):
    member = models.ForeignKey(User, on_delete=models.CASCADE)
    year = models.IntegerField(_('Year'))
    creation_timestamp = models.DateTimeField(auto_now_add=True)
    payment_date = models.DateField(verbose_name=_('Date'), blank=True, null=True)
    payment_amount = models.DecimalField(
        verbose_name=_('Amount'), decimal_places=2, max_digits=6, blank=True, null=True
    )
    notes = models.TextField(_('Notes'), blank=True, null=True)
