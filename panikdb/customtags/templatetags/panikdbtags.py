import datetime

from combo.data.models import Page
from django.conf import settings
from django.db.models import Sum
from django.template import Library
from django.urls import reverse
from emissions.models import Emission, Episode, Focus, NewsItem, SoundFile

from panikdb.stats.models import DailyStat

register = Library()


@register.filter
def online_url(object):
    if isinstance(object, (NewsItem, Emission, Episode)):
        return object.get_site_url()
    if isinstance(object, SoundFile):
        return (
            settings.WEBSITE_BASE_URL
            + 'emissions/'
            + object.episode.emission.slug
            + '/'
            + object.episode.slug
        )
    if isinstance(object, Page):
        return settings.WEBSITE_BASE_URL + object.get_online_url().strip('/')
    return ''


@register.filter
def has_focus(object):
    if isinstance(object, NewsItem):
        object_type = 'newsitem'
    elif isinstance(object, Emission):
        object_type = 'emission'
    elif isinstance(object, Episode):
        object_type = 'episode'
    elif isinstance(object, SoundFile):
        object_type = 'soundfile'
    elif isinstance(object, Page):
        object_type = 'page'
    else:
        return False
    try:
        focus = Focus.objects.get(**{object_type: object})
    except Focus.DoesNotExist:
        return False
    return focus.current == True


@register.filter
def unset_focus_url(object):
    if isinstance(object, NewsItem):
        object_type = 'newsitem'
    elif isinstance(object, Emission):
        object_type = 'emission'
    elif isinstance(object, Episode):
        object_type = 'episode'
    elif isinstance(object, SoundFile):
        object_type = 'soundfile'
    elif isinstance(object, Page):
        object_type = 'page'
    return reverse('focus-unset', kwargs={'object_type': object_type, 'object_id': object.id})


@register.filter
def set_focus_url(object):
    if isinstance(object, NewsItem):
        object_type = 'newsitem'
    elif isinstance(object, Emission):
        object_type = 'emission'
    elif isinstance(object, Episode):
        object_type = 'episode'
    elif isinstance(object, SoundFile):
        object_type = 'soundfile'
    elif isinstance(object, Page):
        object_type = 'page'
    return reverse('focus-set', kwargs={'object_type': object_type, 'object_id': object.id})


@register.filter
def can_focus(object):
    if isinstance(object, NewsItem):
        return bool(object.category and object.image)
    elif isinstance(object, Emission):
        return True
    elif isinstance(object, Episode):
        return True
    elif isinstance(object, SoundFile):
        return bool(object.format)
    elif isinstance(object, Page):
        return bool(object.picture)
    return False


@register.filter
def is_page(obj):
    return isinstance(obj, Page)


@register.filter
def nb_visits(obj):
    if obj.creation_timestamp.date() >= datetime.date(2021, 1, 26):
        count = obj.download_count
    else:
        count = DailyStat.objects.filter(soundfile_id=obj.id).aggregate(Sum('nb_visits'))['nb_visits__sum']
    return count or '-'
