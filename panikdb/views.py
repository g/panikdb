import datetime
import logging

import emissions.views
import newsletter.views
import pkg_resources
from agendas.models import Agenda
from combo.data.models import Page
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect
from django.template import loader
from django.urls import reverse
from django.views.generic.base import RedirectView, TemplateView
from django.views.generic.list import ListView
from emissions.models import Diffusion, Emission, Episode, Focus, NewsCategory, NewsItem, SoundFile
from emissions.utils import period_program

from .pige.views import PigeDownloadMixin
from .poll.models import Vote
from .service_messages.models import Message

logger = logging.getLogger('panikdb')


class Home(TemplateView, PigeDownloadMixin):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update(self.get_pige_context_data())

        try:
            vote = Vote.objects.get(user=self.request.user)
        except Vote.DoesNotExist:
            context['display_poll_button'] = True
        context['emissions'] = self.request.user.emissions.all().order_by('archived', 'title')
        context['news_categories'] = self.request.user.news_categories.all().order_by('title')
        context['service_message'] = Message.objects.all().first()
        if not context['service_message']:
            context['service_message'] = Message.objects.create(style='info')
        if not context['service_message'].text and not self.request.user.has_perm(
            'service_messages.change_message'
        ):
            context['service_message'] = None
        if self.request.user.has_perm('emissions.add_focus'):
            context['focused_items'] = Focus.objects.select_related().filter(current=True)
            context['recent_episodes'] = Episode.objects.select_related().order_by('-last_update_timestamp')
            context['recent_newsitems'] = NewsItem.objects.select_related().order_by('-last_update_timestamp')
            context['recent_soundfiles'] = SoundFile.objects.select_related().order_by(
                '-last_update_timestamp'
            )
        context['extra_top_links'] = settings.HOME_EXTRA_TOP_LINKS
        context['membership_help_url'] = settings.MEMBERSHIP_HELP_URL
        context['has_agendas'] = bool(Agenda.objects.all().exists())

        return context


home = login_required(Home.as_view())


class Search(TemplateView):
    template_name = 'search/search.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = self.request.GET.get('q', '')
        vector = (
            SearchVector('title', config=settings.FTS_DICTIONARY_CONFIG, weight='A')
            + SearchVector('subtitle', config=settings.FTS_DICTIONARY_CONFIG, weight='A')
            + SearchVector('text', config=settings.FTS_DICTIONARY_CONFIG, weight='B')
        )
        query = SearchQuery(self.request.GET.get('q', ''), config=settings.FTS_DICTIONARY_CONFIG)
        context['hits'] = {
            'episodes': Episode.objects.annotate(rank=SearchRank(vector, query))
            .filter(rank__gte=0.1)
            .select_related('emission')
            .order_by('-rank')[:10],
            'emissions': Emission.objects.annotate(rank=SearchRank(vector, query))
            .filter(rank__gte=0.1)
            .order_by('-rank')[:10],
            'newsitems': NewsItem.objects.annotate(rank=SearchRank(vector, query))
            .filter(rank__gte=0.1)
            .order_by('-rank')[:10],
        }
        return context


search = Search.as_view()


class EmissionListView(emissions.views.EmissionListView):
    pass


emissions_list = login_required(EmissionListView.as_view())


class NewsletterCreateView(newsletter.views.NewsletterCreateView):
    def get_initial(self):
        initial = super().get_initial()

        newsletter_body = loader.get_template('newsletter/body.html')
        newsletter_subject = loader.get_template('newsletter/subject.txt')
        context = {}

        date = datetime.datetime.today()
        if date.weekday() < 3:
            # before and on Wednesday, use current week
            date = date - datetime.timedelta(days=date.weekday())
        elif date.weekday():
            # after Wednesday, use next week
            date = date + datetime.timedelta(days=7 - date.weekday())

        date = datetime.datetime(*date.timetuple()[:3])
        date_end = date + datetime.timedelta(days=7)

        context['date'] = date
        context['date_end'] = date_end

        context['soundfiles'] = (
            SoundFile.objects.published()
            .filter(fragment=False)
            .select_related()
            .filter(creation_timestamp__gte=date - datetime.timedelta(days=7))
        )

        context['diffusions'] = list(
            Diffusion.objects.select_related()
            .filter(datetime__gte=date, datetime__lte=date + datetime.timedelta(days=7))
            .order_by('datetime')
        )

        # remove rebroadcasts made during the same week
        seen_episodes = {}
        for diff in context['diffusions'][:]:
            if diff.episode.id in seen_episodes:
                context['diffusions'].remove(diff)
            seen_episodes[diff.episode.id] = True

        initial['subject'] = newsletter_subject.render(context)
        initial['text'] = newsletter_body.render(context)
        return initial


newsletter_create = NewsletterCreateView.as_view()


class FocusSetView(RedirectView):
    permanent = False

    def get_redirect_url(self, object_type, object_id):
        if not self.request.user.has_perm('emissions.add_focus'):
            raise PermissionDenied()
        messages.success(self.request, emissions.views.SUCCESS_MESSAGE)
        if object_type == 'emission':
            emission = Emission.objects.get(id=object_id)
            f, _created = Focus.objects.get_or_create(emission=emission)
            f.current = True
            f.save()
            logger.info('set focus on emission %s', emission)
            return reverse('emission-view', kwargs={'slug': emission.slug})
        if object_type == 'episode':
            episode = Episode.objects.get(id=object_id)
            f, _created = Focus.objects.get_or_create(episode=episode)
            f.current = True
            f.episode = episode
            f.save()
            logger.info('set focus on episode %s', episode)
            return reverse(
                'episode-view', kwargs={'emission_slug': episode.emission.slug, 'slug': episode.slug}
            )
        if object_type == 'newsitem':
            newsitem = NewsItem.objects.get(id=object_id)
            f, _created = Focus.objects.get_or_create(newsitem=newsitem)
            f.current = True
            f.newsitem = newsitem
            f.save()
            logger.info('set focus on newsitem %s', newsitem)
            return reverse('newsitem-view', kwargs={'slug': newsitem.slug})
        if object_type == 'soundfile':
            soundfile = SoundFile.objects.get(id=object_id)
            f, _created = Focus.objects.get_or_create(soundfile=soundfile)
            f.current = True
            f.soundfile = soundfile
            f.save()
            logger.info('set focus on sound %s', soundfile)
            return reverse(
                'episode-view',
                kwargs={
                    'slug': soundfile.episode.slug,
                    'emission_slug': soundfile.episode.emission.slug,
                },
            )
        if object_type == 'page':
            page = Page.objects.get(id=object_id)
            try:
                f = Focus.objects.get(page=page)
            except Focus.DoesNotExist:
                f = Focus()
            f.current = True
            f.page = page
            f.save()
            logger.info('set focus on page %s', page)
            return reverse('combo-manager-page-view', kwargs={'pk': object_id})


focus_set = FocusSetView.as_view()


class FocusUnsetView(RedirectView):
    permanent = False

    def get_redirect_url(self, object_type, object_id):
        if not self.request.user.has_perm('emissions.delete_focus'):
            raise PermissionDenied()
        messages.success(self.request, emissions.views.SUCCESS_MESSAGE)
        if object_type == 'emission':
            emission = Emission.objects.get(id=object_id)
            focus = Focus.objects.get(emission=emission)
            focus.current = False
            focus.save()
            logger.info('unset focus of emission %s', emission)
            return reverse('emission-view', kwargs={'slug': emission.slug})
        if object_type == 'episode':
            episode = Episode.objects.get(id=object_id)
            focus = Focus.objects.get(episode=episode)
            focus.current = False
            focus.save()
            logger.info('unset focus of episode %s', episode)
            return reverse(
                'episode-view', kwargs={'emission_slug': episode.emission.slug, 'slug': episode.slug}
            )
        if object_type == 'newsitem':
            newsitem = NewsItem.objects.get(id=object_id)
            focus = Focus.objects.get(newsitem=newsitem)
            focus.current = False
            focus.save()
            logger.info('unset focus of newsitem %s', newsitem)
            return reverse('newsitem-view', kwargs={'slug': newsitem.slug})
        if object_type == 'soundfile':
            soundfile = SoundFile.objects.get(id=object_id)
            focus = Focus.objects.get(soundfile=soundfile)
            focus.current = False
            focus.save()
            logger.info('unset focus of soundfile %s', soundfile)
            return reverse(
                'episode-view',
                kwargs={
                    'slug': soundfile.episode.slug,
                    'emission_slug': soundfile.episode.emission.slug,
                },
            )
        if object_type == 'page':
            focus = Focus.objects.get(page=object_id)
            focus.current = False
            focus.save()
            logger.info('unset focus of page %s', object_id)
            return reverse('combo-manager-page-view', kwargs={'pk': object_id})


focus_unset = FocusUnsetView.as_view()


class System(TemplateView):
    template_name = 'system.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pkg_resources_set'] = pkg_resources.WorkingSet()
        return context


system = login_required(System.as_view())
