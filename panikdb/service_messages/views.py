from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.views.generic.edit import UpdateView

from .models import Message


class MessageEditView(UpdateView):
    model = Message
    fields = ['style', 'text']
    success_url = '/'
    template_name = 'service_message_form.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.has_perm('service_messages.change_message'):
            raise PermissionDenied()
        return super().dispatch(*args, **kwargs)


message_edit = login_required(MessageEditView.as_view())
