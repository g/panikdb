from django.urls import path

from . import views

urlpatterns = [
    path('edit/<int:pk>/', views.message_edit, name='message-edit-view'),
]
