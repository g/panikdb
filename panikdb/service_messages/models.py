from ckeditor.fields import RichTextField
from django.db import models
from django.utils.translation import gettext_lazy as _

MESSAGE_STYLES = (
    ('info', _('Info')),
    ('error', _('Alert')),
)


class Message(models.Model):
    style = models.CharField(_('Style'), max_length=20, default='info', choices=MESSAGE_STYLES)
    text = RichTextField(_('Description'), blank=True, default='')
