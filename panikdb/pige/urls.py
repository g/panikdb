from django.urls import path, re_path

from . import views

urlpatterns = [
    path('download/', views.download, name='pige-download-post'),
    path('download/pending/<int:object_id>/', views.download_pending),
    re_path(r'^download/(?P<spec>[\w\d\.-]+)$', views.download_spec, name='pige-download-spec'),
]
