L’enregistrement est disponible à l’adresse

  {{ website_url }}/media/pige/{{ pige_request.get_email_download_spec }}

Il sera automatiquement supprimé d’ici cinq jours.
