import datetime
import os

import psutil
from django.conf import settings
from django.core.files.storage import default_storage
from django.core.mail import send_mail
from django.core.management.base import BaseCommand
from django.template import loader
from django.utils import translation
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from panikdb.pige.models import PigeRequest
from panikdb.pige.utils import create_process_from_spec


class Command(BaseCommand):
    def handle(self, verbosity, **kwargs):
        # lowest priority
        process = psutil.Process()
        process.nice(20)
        process.ionice(psutil.IOPRIO_CLASS_IDLE)

        translation.activate(settings.LANGUAGE_CODE)
        pige_path = default_storage.path('pige')
        for pige_request in PigeRequest.objects.filter(mode='sendmail', status=''):
            spec = pige_request.get_email_download_spec()
            download_path = os.path.join(pige_path, spec)
            if not os.path.exists(download_path):
                if verbosity:
                    print('creating', download_path)
                pige_request.download_timestamp = now()
                try:
                    with create_process_from_spec(spec, output=download_path) as proc:
                        proc.wait()
                    if proc.returncode != 0:
                        pige_request.status = 'download-error (%s)' % proc.returncode
                    else:
                        pige_request.status = 'downloaded'
                except Exception as e:
                    pige_request.status = 'process-error (%s)' % e
            else:
                pige_request.download_timestamp = now()
                pige_request.status = 'downloaded'  # was already there
            pige_request.save()

        for pige_request in PigeRequest.objects.filter(
            status='downloaded', download_timestamp__lt=now() - datetime.timedelta(minutes=10)
        ):
            subject = '%s - %s' % (settings.SITE_TITLE, _('record download'))
            body = loader.get_template('pige-email.txt')
            context = {
                'pige_request': pige_request,
                'website_url': settings.WEBSITE_BASE_URL.strip('/'),
            }
            try:
                send_mail(subject, body.render(context), from_email=None, recipient_list=[pige_request.email])
                pige_request.status = 'sent'
            except Exception as e:
                print(e)
                pige_request.status = 'sent-error'
            pige_request.save()

        for pige_request in PigeRequest.objects.filter(
            status='sent', download_timestamp__lt=now() - datetime.timedelta(days=5)
        ):
            download_path = os.path.join(pige_path, pige_request.get_email_download_spec())
            if os.path.exists(download_path):
                os.unlink(download_path)
            pige_request.delete()
