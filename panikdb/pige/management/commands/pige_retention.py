import datetime
import os
import re
import subprocess
import time

import psutil
from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, verbosity, **kwargs):
        # lowest priority
        process = psutil.Process()
        process.nice(20)
        process.ionice(psutil.IOPRIO_CLASS_IDLE)

        self.verbosity = verbosity
        for pige in settings.PIGES or []:
            if not pige.get('retention_policy'):
                continue
            self.apply_policy(pige['base_dir'], pige['retention_policy'])

    def apply_policy(self, base_dir, retention_policy):
        today = datetime.date.today()
        for basedir, dirnames, filenames in os.walk(base_dir):
            try:
                directory_date = datetime.datetime.strptime(basedir[len(base_dir) :], '%Y/%m/%d').date()
            except ValueError:
                continue
            if retention_policy.get('remove_silent_files'):
                self.remove_silent_files([os.path.join(basedir, x) for x in filenames])
            policies = [
                (duration, policy)
                for policy, duration in retention_policy.items()
                if directory_date < today - datetime.timedelta(days=duration)
                and policy != 'remove_silent_files'
            ]
            policies.sort()
            if not policies:
                continue
            policy_name, policy_args = policies[-1][1], ''
            if ':' in policy_name:
                policy_name, policy_args = policy_name.split(':', 1)
            policy_method = getattr(self, 'apply_policy_' + policy_name)
            for filename in filenames:
                if filename.startswith('tmp_'):
                    continue
                policy_method(os.path.join(basedir, filename), policy_args=policy_args)

    def apply_policy_flac(self, filename, policy_args):
        if os.path.splitext(filename)[-1] not in ('.wav',):
            return
        if self.verbosity:
            print('applying flac policy to %s' % filename)
        basename = os.path.splitext(os.path.basename(filename))[0] + '.flac'
        tmp_filename = os.path.join(os.path.dirname(filename), 'tmp_' + basename)
        cmd = ['flac', '--silent', '--force'] + (policy_args or '').split() + ['-o', tmp_filename, filename]
        if self.verbosity > 1:
            del cmd[1]
        subprocess.run(cmd, check=True)
        os.rename(tmp_filename, os.path.join(os.path.dirname(filename), basename))
        os.unlink(filename)

    def apply_policy_ogg(self, filename, policy_args):
        if os.path.splitext(filename)[-1] not in ('.flac', '.wav'):
            return
        if self.verbosity:
            print('applying ogg policy to %s' % filename)
        basename = os.path.splitext(os.path.basename(filename))[0] + '.ogg'
        tmp_filename = os.path.join(os.path.dirname(filename), 'tmp_' + basename)
        if not policy_args:
            policy_args = '-q 6'
        cmd = ['oggenc', '--quiet'] + policy_args.split() + ['-o', tmp_filename, filename]
        if self.verbosity > 1:
            del cmd[1]
        subprocess.run(cmd, check=True)
        os.rename(tmp_filename, os.path.join(os.path.dirname(filename), basename))
        os.unlink(filename)

    def apply_policy_delete(self, filename, policy_args):
        if self.verbosity:
            print('applying delete policy to %s' % filename)
        os.unlink(filename)

    def remove_silent_files(self, filenames):
        now = time.time()

        for filename in filenames:
            if os.path.splitext(filename)[-1] not in ('.wav', '.flac', '.ogg'):
                continue
            if os.stat(filename).st_mtime > (now - 15 * 60):
                continue
            mark_filename = os.path.join(os.path.splitext(filename)[0] + '.checked')
            if os.path.exists(mark_filename):
                continue
            try:
                lines = subprocess.run(
                    ['sox', filename, '-n', 'stats'], capture_output=True, check=True, text=True
                ).stderr.splitlines()
            except subprocess.CalledProcessError:
                if self.verbosity:
                    print('failed to get audio stats for', filename)
                continue
            lines = [x for x in lines if x.startswith('Pk lev dB')]
            if not lines:
                if self.verbosity:
                    print('failed to get peak level for', filename)
                continue
            if lines[0].count('-inf') == 3:
                if self.verbosity:
                    print('total silence (probably unconnectd jack?)', filename)
                os.unlink(filename)
                continue
            max_db = max(float(x) for x in re.findall(r'[0-9\.-]+', lines[0]))
            if max_db < -60:
                if self.verbosity:
                    print('removing silent file', filename, max_db)
                os.unlink(filename)
            else:
                with open(mark_filename, 'w') as fd:
                    fd.write(lines[0])
