import datetime
from functools import partial

from django.conf import settings
from django.contrib import messages
from django.http import HttpResponseNotAllowed, HttpResponseRedirect, StreamingHttpResponse
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from .models import PigeRequest
from .utils import create_process_from_spec, is_internal


class PigeDownloadMixin:
    def get_pige_context_data(self):
        context = {}
        if settings.PIGES:
            context['pige_minimum_date'] = datetime.datetime.now() - datetime.timedelta(days=100)
            context['pige_hours'] = ['%02d' % x for x in range(24)]
            context['pige_minutes'] = ['%02d' % x for x in range(0, 60, 15)]
        return context


def download(request, *args, **kwargs):
    if request.method != 'POST':
        return HttpResponseNotAllowed(['POST'])
    # studio2-20160515-13h45-14h00m05.flac
    name = '%(src)s-%(date)s-%(start_hour)sh%(start_min)s-%(end_hour)sh%(end_min)s' % {
        'src': request.POST['src'],
        'date': request.POST['date'].replace('-', ''),
        'start_hour': request.POST['start_hour'],
        'start_min': request.POST['start_min'],
        'end_hour': request.POST['end_hour'],
        'end_min': request.POST['end_min'],
    }
    if is_internal(request):
        name = name + '.wav'
    else:
        name = name + '.ogg'
    if 'sendmail' in request.POST:
        email = request.POST['email']
        PigeRequest.objects.create(email=email, download_spec=name, mode='sendmail')
        messages.info(
            request,
            _(
                'An email with a download link will soon be sent to %s, '
                'please be patient, it may take up to 20 minutes to arrive.'
            )
            % email,
        )
        return HttpResponseRedirect(reverse('regie-home'))
    else:  # direct download
        return HttpResponseRedirect(reverse('pige-download-spec', kwargs={'spec': name}))


def download_spec(request, spec, *args, **kwargs):
    proc = create_process_from_spec(spec)
    if spec.endswith('.wav'):
        content_type = 'audio/x-wav'
    else:
        content_type = 'audio/ogg'
    response = StreamingHttpResponse(iter(partial(proc.stdout.read, 500_000), b''), content_type=content_type)
    response['Content-Disposition'] = f'attachment; filename={spec}'
    return response


def download_pending(request, *args, **kwargs):
    pass
