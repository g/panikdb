import datetime
import os
import subprocess

from django.conf import settings

from panikdb.context_processors import internal_ip

PATH_LAYOUT = '%Y/%m/%d/'
FILENAME_LAYOUT = '%Hh%M'


def create_process_from_spec(spec, output='-', *args, **kwargs):
    # spec has to be of date-start-end.extension
    #  - date must be %Y%m%d
    #  - start and end must be one of %Hh%Mm%S.%f, %Hh%Mm%S, %Hh%Mm, %Hh%M
    #  - extension must be one of wav, ogg, flac
    #
    # ex: 20160515-13h45-14h00m05.flac
    #
    # it can also be prefixed with the source identifier,
    # ex: studio2-20160515-13h45-14h00m05.flac
    # ex: pige-20220703-13h30-14h00.ogg

    basename, ext = spec.rsplit('.', 1)
    if basename.count('-') == 2:
        date_str, start_str, end_str = basename.split('-')
        src_id = settings.PIGES[0]['slug']
    else:
        src_id, date_str, start_str, end_str = basename.split('-')

    date = datetime.datetime.strptime(date_str, '%Y%m%d')

    start_time = None
    end_time = None
    for time_format in ('%Hh%Mm%S.%f', '%Hh%Mm%S', '%Hh%Mm', '%Hh%M'):
        if start_time is None:
            try:
                start_time = datetime.datetime.strptime(start_str, time_format)
            except ValueError:
                pass
        if end_time is None:
            try:
                end_time = datetime.datetime.strptime(end_str, time_format)
            except ValueError:
                pass
    start = datetime.datetime(
        date.year,
        date.month,
        date.day,
        start_time.hour,
        start_time.minute,
        start_time.second,
        start_time.microsecond,
    )
    end = datetime.datetime(
        date.year, date.month, date.day, end_time.hour, end_time.minute, end_time.second, end_time.microsecond
    )

    if end <= start:
        end = end + datetime.timedelta(1)

    floor_start = start
    if floor_start.second or floor_start.microsecond:
        floor_start = floor_start.replace(second=0, microsecond=0)
    floor_start = floor_start.replace(minute=floor_start.minute // 15 * 15)

    ceil_end = end
    if ceil_end.second or ceil_end.microsecond:
        ceil_end = ceil_end.replace(minute=ceil_end.minute + 1, second=0, microsecond=0)
    if ceil_end.minute % 15:
        if ceil_end.minute // 15 == 3:
            ceil_end = ceil_end.replace(minute=0)
            ceil_end += datetime.timedelta(seconds=3600)
        else:
            ceil_end = ceil_end.replace(minute=(1 + (ceil_end.minute // 15)) * 15)

    # get filenames
    base_dir = [x for x in settings.PIGES if x['slug'] == src_id][0]['base_dir']
    path = os.path.join(base_dir, start.strftime(PATH_LAYOUT))
    if not path:
        filenames = [x for x in os.listdir('.')]
    elif os.path.exists(path):
        filenames = [os.path.join(path, x) for x in os.listdir(path)]
        if end.hour < start.hour:
            path = os.path.join(base_dir, end.strftime(PATH_LAYOUT))
            if os.path.exists(path):
                filenames.extend([os.path.join(path, x) for x in os.listdir(path)])
    else:
        raise Exception('missing directory: %s' % path)

    filenames = [x for x in filenames if os.path.splitext(x)[-1] in ('.ogg', '.flac', '.wav')]
    filenames = [
        x
        for x in filenames
        if x[len(base_dir) :] >= floor_start.strftime(PATH_LAYOUT + FILENAME_LAYOUT)
        and x[len(base_dir) :] < ceil_end.strftime(PATH_LAYOUT + FILENAME_LAYOUT)
    ]
    filenames.sort()

    if ext == 'ogg':
        command = ['/usr/bin/sox'] + filenames + ['-t', 'ogg', '-C', '6', output]
    elif ext == 'flac':
        command = ['/usr/bin/sox'] + filenames + ['-t', 'flac', output]
    else:
        command = ['/usr/bin/sox'] + filenames + ['-t', 'wav', output]

    # trim
    command.append('trim')
    command.append('=%.6f' % (start - floor_start).total_seconds())
    command.append('=%.6f' % (end - floor_start).total_seconds())

    if kwargs.get('fade'):  # fade duration, ex: 0.2
        command.append('fade')
        command.append(kwargs['fade'])
        command.append('0')  # apply fade to both start and end

    if kwargs.get('norm'):  # normalize
        command.append('gain')
        command.append('-n')
        command.append(kwargs['norm'])

    return subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)


def is_internal(request):
    return internal_ip(request).get('internal_ip')
