from django.db import models
from django.utils.translation import gettext_lazy as _


class PigeRequest(models.Model):
    mode = models.CharField(
        _('Mode'),
        max_length=20,
        default='download',
        blank=False,
        choices=[
            ('download', _('Download')),
            ('email', _('Email')),
        ],
    )
    email = models.CharField(max_length=255)
    download_spec = models.CharField(max_length=255)
    # eg: studio2-20160515-13h45-14h00m05.flac
    status = models.CharField(max_length=100, default='')
    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    download_timestamp = models.DateTimeField(null=True)

    def get_email_download_spec(self):
        # for emails, always give the .ogg
        return self.download_spec.replace('.wav', '.ogg')
