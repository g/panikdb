#! /usr/bin/env python3

import glob
import os
import subprocess
import sysconfig


def system_install():
    packages = '''
gettext
graphicsmagick
inkscape
make
memcached
npm
patch
postgresql
pre-commit
python3-aiohttp
python3-atomicwrites
python3-cffi
python3-cryptography
python3-defusedxml
python3-http-ece
python3-lasso
python3-lxml
python3-pil
python3-psycopg2
python3-pycryptodome
python3-pyproj
python3-requests
sassc
zip
'''.splitlines()
    subprocess.run(['sudo', 'apt', '--no-install-recommends', 'install'] + packages, check=True)


def pip_system_links():
    dir = sysconfig.get_path('purelib')
    patterns = '''
aiohttp*
atomicwrites*
cffi*
_cffi*
cryptography*
Cryptodome
pycryptodomex*
defusedxml*
http_ece*
lxml*
PIL
pillow*
psycopg2*
pyproj*
requests*
'''.strip().splitlines()
    for pattern in patterns:
        for filepath in glob.glob(f'/usr/lib/python3/dist-packages/{pattern}'):
            dst = os.path.join(dir, os.path.basename(filepath))
            if not os.path.exists(dst):
                os.symlink(filepath, dst)


def pip_install_base():
    subprocess.run(['pip', 'install', 'setuptools'], check=True)


def pip_install_xstatic():
    # install xstatic modules first and together,
    # https://github.com/xstatic-py/xstatic/commit/1d687bef3d8cfad66213e8cf9e96e0aa17aae198
    modules = '''
XStatic
XStatic-jQuery
XStatic-jquery-ui
XStatic-Leaflet
XStatic-Leaflet-GestureHandling
XStatic-Leaflet-MarkerCluster
XStatic_OpenSans
XStatic_roboto-fontface>=0.5.0.0
XStatic-Select2
'''.strip().splitlines()
    subprocess.run(['pip', 'install'] + modules, check=True)


def pip_install_django():
    # get latest LTS (4.2.x)
    subprocess.run(['pip', 'install', 'django>4,<5'], check=True)


def pip_install_git_versions():
    # get modules from git.entrouvert.org branches
    modules = '''
debian-django-ckeditor
publik-django-templatetags
gadjo
django-mellon
combo
'''.strip().splitlines()
    deps_dir = os.path.join(os.environ['VIRTUAL_ENV'], 'src')
    if not os.path.exists(deps_dir):
        os.mkdir(deps_dir)
    for module in modules:
        if os.path.exists(os.path.join(deps_dir, module)):
            subprocess.run(['git', 'pull'], cwd=os.path.join(deps_dir, module), check=True)
        else:
            subprocess.run(
                ['git', 'clone', f'https://git.entrouvert.org/entrouvert/{module}.git'],
                cwd=deps_dir,
                check=True,
            )
        subprocess.run(['python3', 'setup.py', 'develop'], cwd=os.path.join(deps_dir, module), check=True)
        subprocess.run(['python3', 'setup.py', 'build'], cwd=os.path.join(deps_dir, module), check=True)


def setup_local_modules():
    for module_dir in glob.glob('../django-panik-*') + ['.']:
        subprocess.run(['python3', 'setup.py', 'develop'], cwd=module_dir, check=True)


if __name__ == '__main__':
    assert os.environ.get('VIRTUAL_ENV'), 'you must run this from a virtual environment'
    system_install()
    pip_system_links()
    pip_install_base()
    pip_install_xstatic()
    pip_install_django()
    pip_install_git_versions()
    setup_local_modules()
